%define anolis_release 1
%define __lib lib
%define __python %(which python3 2>/dev/null)
%define _nonzero_exit_pkgcheck_terminate_build 1
%undefine __brp_mangle_shebangs
%global dist_ver %(rpm -E %{?dist}|awk '{sub(".an","");print}')
%global os_id %(cat /etc/os-release|awk -F'"' '/^ID=/{print $(NF-1)}')

Name:               ancert
Version:            2.0
Release:            %{anolis_release}%{?dist}
Summary:            Anolis Hardware Compatibility Test Suite
License:            MulanPSL2 and BSD
Group:              Development/Libraries
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Vendor:             Alibaba
URL:                https://gitee.com/anolis/ancert
Source:             %{name}-v%{version}.tar.gz
BuildRequires:      make, gcc, gcc-c++
Requires:           make, gcc, gcc-c++, clang
Requires:           fio, nvme-cli, glx-utils, python3, rpm-build, bc, lvm2
Requires:           alsa-lib, alsa-utils, virt-what, smartmontools, hdparm
Requires:           OpenIPMI, ipmitool, freeipmi
Requires:           iproute, iputils, ethtool, iperf3, sysstat, expect, tree, mdadm
%if "%{os_id}" == "anolis"
    %if %{dist_ver} >= 7
        %if %{dist_ver} < 23
Requires:           xorg-x11-utils, xorg-x11-server-utils, xorg-x11-apps, memstrack, stress-ng
            %if %{dist_ver} >= 8
Requires:           python3-pyyaml
            %else
Requires:           python36-PyYAML
            %endif
        %else
Requires:           xorg-x11-server-Xwayland, x11perf, xdpyinfo, xvinfo, xset, python3-pyyaml, memstrack
        %endif
    %else
    %{error: getting dist version,it should be greater than 7.x for Anolis OS}
    %endif
%endif

%if "%{os_id}" != "anolis"
%{error: please build rpm in Anolis OS.}
%endif
ExclusiveArch:      x86_64 aarch64 loongarch64

%description
This test suite to be used to verify your system or component the compatibility with Anolis OS.

%prep
%if "nil%{__python}" == "nil"
%{error: Failed to exec which python3}
%endif
%setup -qn %{name}-v%{version}

%install
make DESTDIR=%{buildroot}%{_datadir}/%{name} post_install

%files
%defattr(-,root,root)
%{_datadir}/%{name}/%{name}
%{_datadir}/%{name}/etc
%{_datadir}/%{name}/%{__lib}
%{_datadir}/%{name}/tests
%{_datadir}/%{name}/utils
%{_datadir}/%{name}/README.md
%{_datadir}/%{name}/LICENSE
%exclude %{_datadir}/%{name}/%{name}.spec

%post
if [ "$1" = "1" ]; then
    [ -f %{_bindir}/%{name} ] || [ -L %{_bindir}/%{name} ] \
        && rm -rf %{_bindir}/%{name}
    ln -s %{_datadir}/%{name}/%{name} %{_bindir}/%{name}
fi

%postun
if [ "$1" = "0" ]; then
    [ -d %{_datadir}/%{name} ] && rm -rf %{_datadir}/%{name}
    [ -L %{_bindir}/%{name} ] && rm -rf %{_bindir}/%{name}
fi

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && [ -d $RPM_BUILD_ROOT ] && rm -rf $RPM_BUILD_ROOT;

%changelog
* Mon Nov 21 2022 chaofengwu <17269747_wcf@163.com> -2.0-1
- update ancert architecture to 2.0

* Tue Aug 16 2022 chaofengwu <17269747_wcf@163.com> -1.2-1
- support IPMI , BIOS test

* Thu Mar 3 2022 chaofengwu <17269747_wcf@163.com> -1.1-1
- fixed several bugs

* Wed Nov 24 2021 chaofengwu <17269747_wcf@163.com> -1.0-1
- support log packaging and test results checksum