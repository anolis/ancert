#!/usr/bin/env bash

PKGS=
BASEDIR=$(cd `dirname $0`; pwd)

function get_all_depend_pkgs() {
    local dist_ver=$(rpm -E %{?dist}|awk '{sub(".an","");print}')

    cd $BASEDIR
    test "$dist_ver" -ge 23 && {
        yum install -y -q anolis-epao-release || { echo "Failed to install anolis-epao-release by yum, please check manually!";exit 1; }
    }
    rpm -qa|grep -E "rpm-build-[0-9]+" || yum install -y rpm-build
    command -v "rpmspec" &>/dev/null || { echo "Failed to install rpm-build, please check manually!";exit 1; }
    # Support different Anolis OS version
    if ! rpmspec -q --requires ancert.spec &>/dev/null;then
        echo "Failed to parse dependency package by it.(rpmspec -q --requires ancert.spec)";exit 1
    fi
    PKGS=$(rpmspec -q --requires ancert.spec|grep -v "/bin")
    echo -e "Get all dependent package information:\n${PKGS}"
}

function install_all_depend_pkgs() {
    get_all_depend_pkgs
    if test -f /etc/os-release;then
        if ! cat /etc/os-release|grep PRETTY_NAME|grep -iwq Anolis;then
            echo "Error: please install it in Anolis OS system!" && exit 1
        fi
    else
        echo "Error: no such file /etc/os-release, please install it in Anolis OS system!" && exit 1
    fi
    echo -e "\ninstall dependent package information:"
    for pkg in $PKGS;do
        yum list installed |grep -Ewq "^`echo $pkg|sed -e 's|\+|\\\+|g'`\.(`arch`|noarch)" && continue ||:
        cmd="yum install ${pkg} -y"
        echo -e "\033[1;40;32m\n${cmd}\033[0m"
        eval $cmd
        [ $? -eq 0 ] || exit 1
    done
}

function source_code_to_install() {
    cd $BASEDIR
    _ancert=$(rpmspec -P ancert.spec|egrep "Name"|awk '{print $NF}')
    [ X"${_ancert}" = X"" ] && echo "Failed to get Name from ancert.spec." && exit 1
    # rpm installation
    if rpm -qa|grep "$_ancert" >/dev/null 2>&1;then
        rpm -e $_ancert || { echo -e "\nFaild to uninstall $_ancert, please execute the command: rpm -e $_ancert";exit 1; }
    else
        # Source unload
        if which ancert &>/dev/null;then
            make uninstall || { echo "Failed to make uninstall";exit 1; }
        fi
    fi
    echo "make install."
    make install || { echo -e "\nSource code installation failed, please execute the command:make uninstall && make install";exit 1; }
    [ -f "`rpm -E %{_datadir}`/${_ancert}/${_ancert}.spec" ] && rm -rf "`rpm -E %{_datadir}`/${_ancert}/${_ancert}.spec"
}

# # # # # # # # # # # # # # main # # # # # # # # # # # #
install_all_depend_pkgs
source_code_to_install
