class NonDriverAttached(RuntimeError):
    pass


class NonAvaliableController(RuntimeError):
    pass


class NonAvaliableDevice(RuntimeError):
    pass


class ConfigError(RuntimeError):
    pass


class RunCommandError(RuntimeError):
    pass
