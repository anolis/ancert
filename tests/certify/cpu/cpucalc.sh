#!/usr/bin/env bash

source ../../lib/shell/common.sh

function show_cpu_info() {
	echo "get cpu information:"
        lscpu
	echo
}

function setup() {
	bc --version &> /dev/null || { echo "bc command is not installed"; test_fail; }
}

function cpucal() {
	echo "cpucal test start!"
	for ((i=1;i<100;i++)); do
                echo "start cycle $i"
		echo "scale=1000; 4*a(1)" | bc -l | md5sum | grep "0e154a0c7df1d0482f86f4083e5c4392">/dev/null || test_fail
	done
}

# # # # # # # # # # main # # # # # # # # # #
show_cpu_info
setup
cpucal
test_pass
