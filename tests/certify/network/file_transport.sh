#!/usr/bin/env bash

source ../../lib/shell/common.sh

function filescp() {
	echo "run network scp file test!"
	lts_ip=$LTS_IPADDRESS
	echo "test lts ip info: $lts_ip"
	echo "scp files start"
	path='/tmp/'
	localfile='scplocal'
	remotefile='scpremote'
	md5value='5392a0941047ab7b364140483b13ca17'
	echo 'openanolis'>$path$localfile || echo "create file failed"

	scp -o "StrictHostKeyChecking no" $path$localfile root@"$lts_ip":$path$remotefile || return 1
	ssh -o "StrictHostKeyChecking no" root@"$lts_ip" "md5sum $path$remotefile" | grep $md5value || return 1
	scp -o "StrictHostKeyChecking no" root@"$lts_ip":$path$remotefile $path$remotefile || return 1
	md5sum $path$remotefile | grep $md5value || return 1

	echo "network file scp function test success!"
	return 0
}

function run_network_test() {
	sh ../../../utils/sshconf.sh "setup"
	if  [ $? -ne 0 ]; then
		test_fail
	fi
	filescp
	if  [ $? -ne 0 ]; then
		sh ../../../utils/sshconf.sh "restore"
		exit 1
	fi
	sh ../../../utils/sshconf.sh "restore"
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
print_test_info "Network"
show_network_info
run_network_test
test_pass
