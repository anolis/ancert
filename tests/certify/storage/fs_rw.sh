#!/usr/bin/env bash

source ../../lib/shell/common.sh

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
print_test_info "Storage"
show_module_info "$MODULE"
if $IS_RAW_DISK; then
	clean_up_disk_partition "$DEVNAME"
fi
show_drive_info
run_fio_test
test_pass
