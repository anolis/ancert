#!/usr/bin/env bash

source ../../lib/shell/common.sh

function run_dd_test() {
	echo "============================dd test DirectIO==============================="
	echo "write to device $DEVNAME with offset"
	run_cmd "dd if=/dev/zero of=$DEVNAME bs=16b count=1k oflag=direct seek=1024"
	echo "read from device $DEVNAME with offset"
	run_cmd "dd if=$DEVNAME of=/dev/null bs=4k count=2048 seek=1024"
	echo "write to device $DEVNAME"
	run_cmd "dd if=/dev/zero of=$DEVNAME bs=512b count=32 oflag=direct"
	echo "read from device $DEVNAME"
	run_cmd "dd if=$DEVNAME of=/dev/null bs=8k count=1024"

	echo "============================dd test BufferIO==============================="
	echo "write from device $DEVNAME"
	run_cmd "dd if=/dev/zero of=$DEVNAME bs=64b count=256 oflag=sync"
	echo "read from device $DEVNAME"
	run_cmd "dd if=$DEVNAME of=/dev/null bs=4k count=2048"
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
if ! $IS_RAW_DISK; then
	echo "env var IS_RAW_DISK should be true, current is $IS_RAW_DISK"
	test_fail
fi
print_test_info "Storage"
show_module_info "$MODULE"
clean_up_disk_partition "$DEVNAME"
show_drive_info
run_dd_test
test_pass
