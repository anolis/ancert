#!/usr/bin/env bash

source ../../lib/shell/common.sh

APEI_IF=`cat /proc/mounts | grep debugfs | cut -d ' ' -f2 | head -1`/apei/einj

function ras_type_check() {
    if ! test -f $APEI_IF/available_error_type;then
        echo "Error, $APEI_IF/available_error_type: No such file." && test_fail
    fi
    grep "0x00000400" $APEI_IF/available_error_type > /dev/null || clean_einj
}

# # # # # # # # # # main # # # # # # # # # #
cpu_ras_setup || exit 1
ras_type_check
test_pass
