#!/usr/bin/env bash

source ../../lib/shell/common.sh

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
echo "lsblk test"
lsblk_ret=$(lsblk | grep disk | awk '{print $1}')
if [ $? -ne 0 ]; then
    write_messages err "lsblk failed."
    test_fail
fi

for disk in ${lsblk_ret};do
    if ! lsscsi | grep -wq "/dev/${disk}";then
        write_messages err "disk: ${disk}, lsblk exist, lsscsi not exist, check failed, pls check,  ret: $ret."
        test_fail
    fi
done

lsscsi_ret=$(lsscsi | awk -F "/dev/" '{print $2}')
for disk in ${lsscsi_ret};do
    if ! lsblk | grep -wq "${disk}";then
        write_messages err "disk: ${disk}, lsscsi exist, lsblk not exist, check failed, pls check, ret: $ret."
        test_fail
    fi
done

echo -e "lsscsi:\n`lsscsi`"
echo -e "lsblk:\n`lsblk | grep disk`"
test_pass
