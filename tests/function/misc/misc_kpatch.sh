#!/bin/bash

source ../../lib/shell/common.sh

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
write_messages info "=========system suspend disk test.============="

function disable_kpatch_service()
{
    local ret

    systemctl disable kpatch.service
    ret=$(echo $?)
    if [ $ret -eq 0 ];then
        write_messages info "kpatch.service disable exec successfully."
    else
        write_messages err "kpatch.service disable exec failed."
        test_fail
    fi
}

function check_disable_kpatch_service()
{
    local ret

    ret=$(systemctl is-enabled kpatch.service)
    if [ "$ret" == "disabled" ];then
        write_messages info "kpatch.service disabled, check pass."
    else
        write_messages err "kpatch.service is not disabled, check failed, ret "$ret"."
        test_fail
    fi
}


function enable_kpatch_service()
{
    local ret

    systemctl enable kpatch.service
    ret=$(echo $?)
    if [ $ret -eq 0 ];then
        write_messages info "kpatch.service enabled exec successfully."
    else
        write_messages err "kpatch.service enabled exec failed."
        test_fail
    fi
}

function check_enable_kpatch_service()
{
    local ret

    ret=$(systemctl is-enabled kpatch.service)
    if [ "$ret" == "enabled" ];then
        write_messages info "kpatch.service enabled, check pass."
    else
        write_messages err "kpatch.service is not enabled, check failed, ret "$ret"."
        test_fail
    fi
}

# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
disable_kpatch_service
check_disable_kpatch_service
enable_kpatch_service
check_enable_kpatch_service
test_pass
