#!/usr/bin/env bash

source ../../lib/shell/common.sh

function nvme_show_queuecpuirq_test() {
    local nvme_dev
    local nvme_controller
    local nvme_mqs
    local nvme_irq
    local nvme_irq_val
    local affinity_cpus
    local irq_smp_affinity_list

    echo "Show nvme_queue affinity_cpu and irq topology info."
    for nd in $NVME_DEVS;do
        nvme_dev="${nd#*/dev/}"
        nvme_controller="${nvme_dev%n*}"
        nvme_mqs="$(ls /sys/block/${nvme_dev}/mq/)"
        [ ! -n "$nvme_mqs" ] && { write_messages err "nvme queue number get failed";test_fail; }
        for num in $nvme_mqs;do
            sleep 1
            affinity_cpus="$(cat /sys/block/${nvme_dev}/mq/${num}/cpu_list)"
            echo "cat /sys/block/${nvme_dev}/mq/${num}/cpu_list--->[${affinity_cpus}]"
            nvme_irq="${nvme_controller}q${num}"
            nvme_irq_val="$(cat /proc/interrupts | grep -w "${nvme_irq}"|awk -F ":" '{print $1}' | sed 's/ //g')"
            [ ! -n "$nvme_irq_val" ] && { write_messages err "nvme dev queue irq number get failed";test_fail; }
            irq_smp_affinity_list="$(cat /proc/irq/${nvme_irq_val//[[:space:]]/}/smp_affinity_list)"
            [ $? -ne 0 ] && { write_messages err "nvme dev queue irq smp_affinity_list get failed";test_fail; }
            echo "nvme_irq: ${nvme_irq}|queue_cpu_list: ${affinity_cpus}|nvme_irq_val: ${nvme_irq_val}|irq_smp_affinity_list: ${irq_smp_affinity_list}"
        done
    done
}
# # # # # # # # # # # # # # main # # # # # # # # # # # # # #
nvme_dev_check
nvme_show_queuecpuirq_test
test_pass
