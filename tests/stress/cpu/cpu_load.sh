#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./cpu_stress.sh

function clean() {
    sleep 5
    echo "clean $CPU_LOAD_LOG"
    test -f "$CPU_LOAD_LOG" && rm -rf "$CPU_LOAD_LOG" ||:
    echo "clean $CPU_LOAD_RESULTS_LOG"
    test -f "$CPU_LOAD_RESULTS_LOG" && rm -rf "$CPU_LOAD_RESULTS_LOG" ||:
    if pgrep cat >/dev/null 2>&1;then
        echo "kill -9 \`pgrep cat\`"
        kill -9 `pgrep cat`
    else
        echo "Nothing to kill for cpu load test."
    fi
}

function cpuload_monitor() {
    local monitor_cpu="$1"
    local m_cpu_num

    check_cmd "mpstat"
    mpstat -P ALL 1 $RUNTIME|grep "Average"|grep -vE "CPU" > $CPU_LOAD_LOG

    if test ! -f $CPU_LOAD_LOG || test ! -s $CPU_LOAD_LOG;then
        echo "Error: no such file $CPU_LOAD_LOG,failed to get Monitoring information." && exit 1
    fi

    for m_cpu in $monitor_cpu;do
        m_cpu_num=$(echo $m_cpu|grep -Eo "[0-9]+")
        echo "Average:     CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle" >>$CPU_LOAD_RESULTS_LOG
        awk -v mnum=$m_cpu_num '{ num=$2;if(num==mnum){print $0} }' $CPU_LOAD_LOG >> $CPU_LOAD_RESULTS_LOG
        echo "" >> $CPU_LOAD_RESULTS_LOG
    done
}

function cpu_load_test() {
    local cur_random
    local cpu_used_nums
    local random_cpu_name=""
    local cur_time_diff=0
    local cur_time="$(date +%s -d "$(date +"%a %b %e %T %Y")")"

    #online cpu info
    echo -e "Get online cpu: \n=============================="
    for on_cpu in $CPU_ONLINE_NAME;do echo "${CPU_PATH}/$on_cpu";done
    echo -e "=============================="
    if test $CPU_ONLINE_NUMS -gt 0 && test $CPU_ONLINE_NUMS -lt 3;then
        cpu_used_nums=1
    elif test $CPU_ONLINE_NUMS -ge 3;then
        #online cpu info, reserve two cpu
        cpu_used_nums=$(echo "$CPU_ONLINE_NUMS - 2"|bc)
    else
        echo "warning: No cpu is online." && test_skip
    fi

    if ! ls /dev/urandom >/dev/null 2>&1;then
        echo "Error: the random pseudo device does not exist" && test_skip
    fi

    for ((i=1;i<=$cpu_used_nums;i++));do
        #random cpu
        while true;do
            cur_random="$(echo $RANDOM%$CPU_ONLINE_NUMS |bc)"
            if ! echo "$random_cpu_name" | grep -wq "cpu${cur_random}";then
                if echo "$CPU_ONLINE_NAME"|grep -wq "cpu${cur_random}";then
                    random_cpu_name="${random_cpu_name}cpu${cur_random} "
                    break
                fi
            else
                continue
            fi
        done
        echo "cpu load test run on cpu${i}, specify to cpu${cur_random}"
        taskset -c ${cur_random} cat /dev/urandom|gzip -9|gzip -d >/dev/null &
    done

    #monitor it
    cpuload_monitor "$random_cpu_name" &
    wait $!

    if test ! -f $CPU_LOAD_RESULTS_LOG;then
        echo "Error: No monitoring log $CPU_LOAD_RESULTS_LOG is generated." && return 1
    fi
    cat $CPU_LOAD_RESULTS_LOG && return 0
}

trap "clean" EXIT
# # # # # # # # # # # # # # main # # # # # # # # # # # #
stress_cpu_env
get_online_cpu || test_fail

cpu_load_test && test_pass || test_fail
