#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/../../lib/shell/env.sh
source $DIR/network_bandwidth_direct.sh

# # # # # # # # main # # # # # # # #
trap "bash ../../../utils/sshconf.sh restore" EXIT
bash ../../../utils/sshconf.sh setup
stress_network_env 5 $RUNTIME 
bandwidth_test && test_pass || test_fail