#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./network_stress.sh

# # # # # # # # main # # # # # # # #
stress_network_env 5 $RUNTIME
IPERF3_CONNECT_MODE="UDP"
IPERF3_INTERVAL=$(echo "$RUNTIME / 10"|bc)
trap "sh ./../../../utils/sshconf.sh restore" EXIT

# The default ssh interface connects to the iperf3 service by setting the UDP connection mode
run_iperf3_stress_test && test_pass || test_fail