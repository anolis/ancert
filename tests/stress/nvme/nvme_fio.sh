#!/usr/bin/env bash

source ../../lib/shell/common.sh

function check_io_speed() {
    local cur_io_dev=$1
    local io_rw_speed=$(sar -dp 1 10 |grep -w "$cur_io_dev"|awk '/Average/{print $2" "$4" " $5}')
    local io_dev_name=$(echo $io_rw_speed|awk '{print $1}')
    local io_read_speed=$(echo $io_rw_speed|awk '{print $2}')
    local io_write_speed=$(echo $io_rw_speed|awk '{print $3}')
    echo "[I/O SPEED]: $io_rw_speed"
    [ $FIO_MIX -ne 0 ] && FIO_STATUS_LOG="${FIO_NVME_LOGPATH}/nvme_fio_mix_${FIO_MODE}.status"
    if [ $(echo "$io_read_speed > $DEFAULT_SPEED"|bc) -eq 1 ] || [ $(echo "$io_write_speed > $DEFAULT_SPEED"|bc) -eq 1 ];then
        echo "${cur_io_dev##*/} 0" >> $FIO_STATUS_LOG
    else
        echo "${cur_io_dev##*/} 1" >> $FIO_STATUS_LOG
    fi
}

function check_results(){
    #check fio exec log and fio status log
    local error_msg=""
    [ $FIO_MIX -ne 0 ] && FIO_STATUS_LOG="${FIO_NVME_LOGPATH}/nvme_fio_mix_${FIO_MODE}.status"
    [ ! -f "${FIO_STATUS_LOG}" ] && echo "Error: No IO is generated" && test_fail
    for fr_d in ${NVME_FREE_DISK[*]};do
        if [ $FIO_MIX -ne 0 ];then
            local fio_nvme_log="${FIO_NVME_LOGPATH}/${fr_d##*/}_mix_${FIO_MODE}.fio"
        else
            local fio_nvme_log="${FIO_NVME_LOGPATH}/${fr_d##*/}_${FIO_MODE}.fio"
        fi
        local fio_st=$(cat $FIO_STATUS_LOG |grep -w "${fr_d##*/}"|awk '{print $NF}')

        [ "$fio_st" -ne 0 ] && error_msg="${error_msg}${fr_d} I/O is abnormal.\n"
        [ ! -f "$fio_nvme_log" ] && error_msg="${error_msg}${fr_d} Error:no I/O is generated.\n" && continue
        echo -e "\n==========[nvme fio log]=========="
        cat $fio_nvme_log
        echo "==========[nvme fio log]=========="
        if ! cat $fio_nvme_log |grep -wq "Disk stats";then
            error_msg="${error_msg}${fr_d} I/O process failure.\n"
        fi
        [ -f "$FIO_STATUS_LOG" ] && rm -rf "$FIO_STATUS_LOG"
        [ -f "$fio_nvme_log" ] && rm -rf $fio_nvme_log
    done

    if [ -n "$error_msg" ];then
        echo -e "$error_msg"
        return 1
    else
        return 0
    fi
}

function nvme_fio_test(){
    local cmds="fio sar"
    local fio_nvme_log=""
    local dev_size=""
    echo -e "\n[start nvme fio test--->${FIO_MODE}]"
    [ -f "$FIO_STATUS_LOG" ] && rm -rf "$FIO_STATUS_LOG"
    for cmd in $cmds;do
        check_cmd "$cmd"
    done

    get_nvme_free_disk
    if [ $? -eq 0 ];then        
        for fr_d in ${NVME_FREE_DISK[*]};do
            [ $FIO_MIX -ne 0 ] && fio_nvme_log="${FIO_NVME_LOGPATH}/${fr_d##*/}_mix_${FIO_MODE}.fio" || fio_nvme_log="${FIO_NVME_LOGPATH}/${fr_d##*/}_${FIO_MODE}.fio"
            [ -f $fio_nvme_log ] && rm -rf $fio_nvme_log
            clean_up_disk_partition "$fr_d"
            dev_size="$(lsblk -pln -o NAME,SIZE|grep -wE $fr_d|awk '{print $NF}')"
            FIO_SIZE=$(echo $dev_size|sed 's/\.[0-9]\+//g')
            cmdlines="fio -filename=${fr_d} -ioengine=sync -iodepth=4 -numjobs=${FIO_NUMJOBS} -group_reporting=1 -size=${FIO_SIZE} -direct=1 -runtime=${FIO_RUNTIME} -rw=${FIO_MODE} -rwmixread=${FIO_MIX} -bs=4k -name=iops_${FIO_MODE}"
            run_cmd "$cmdlines" "$fio_nvme_log" &
            check_io_speed "${fr_d##*/}" &
        done
        wait
        check_results
        [ $? -ne 0 ] && return 1 || return 0
    fi
    return 1
}

# # # # # # # # main # # # # # # # #
print_test_info "NVMe"
show_module_info "$DRIVER"
